# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import absolute_import

from django.core.cache import caches
from django.http.response import HttpResponse
import subprocess
import zipfile

import atexit
import pyoo

from pkg_resources import resource_filename  # @UnresolvedImport
from functools import wraps
import tempfile
from django.template import Template, Context


def memoize(func, cache, num_args):
    """
    Wrap a function so that results for any argument tuple are stored in
    'cache'. Note that the args to the function must be usable as dictionary
    keys.

    Only the first num_args are considered when creating the key.
    """
    @wraps(func)
    def wrapper(*args):
        mem_args = args[:num_args]
        if mem_args in cache:
            return cache[mem_args]
        result = func(*args)
        cache[mem_args] = result
        return result
    return wrapper


class NO_CACHE_ELEMENT:
    pass
NO_CACHE_ELEMENT = NO_CACHE_ELEMENT()


class CalculationCache(dict):
    
    def fmt_key(self, key):
        return 'beregner_' + str(key)
    
    def __init__(self, *args, **kwargs):
        self.django_cache = caches['default']
        dict.__init__(self, *args, **kwargs)
    
    def __setitem__(self, key, value, *args, **kwargs):
        self.django_cache.set(self.fmt_key(key), value)
        return dict.__setitem__(self, key, value, *args, **kwargs)

    def __getitem__(self, key, *args, **kwargs):
        try:
            return dict.__getitem__(self, key, *args, **kwargs)
        except KeyError:
            from_cache = self.django_cache.get(self.fmt_key(key), NO_CACHE_ELEMENT)
            if from_cache is NO_CACHE_ELEMENT:
                raise KeyError


# RATHER EXPLICIT THAN IMPLICIT, USE UNDERSTANDABLE CONSTANTS FOR STUFF

# Types containing different calculation models
(
    COL_ENLIGE_U_30_U_BOERN,
    COL_ENLIGE_O_30_U_BOERN,
    COL_ENLIGE_BARN_PAA_1,
    COL_BOERN_PAA_1_OG_5,
    COL_BOERN_PAA_1_5_10,
) = (2, 3, 4, 5, 6)

# Rows containing different instances of the calculation made in each column
(
    ROW_CURRENT_KONTANTHJ,
    ROW_INPUT_KONTANTHJ,
    ROW_OUTPUT_KONTANTHJ,
    ROW_CURRENT_BOLIGSIKRING,
    ROW_INPUT_BOLIGSIKRING,
    ROW_OUTPUT_BOLIGSIKRING,
    ROW_CURRENT_BOERNEYDELSE,
    ROW_INPUT_BOERNEYDELSE,
    ROW_OUTPUT_BOERNEYDELSE,
    ROW_CURRENT_FRIPLADSTILSKUD,
    ROW_INPUT_FRIPLADSTILSKUD,
    ROW_OUTPUT_FRIPLADSTILSKUD,
    ROW_CURRENT_SAMLET_YDELSE,
    ROW_OUTPUT_SAMLET_YDELSE,
    ROW_CURRENT_RAADIGHEDSBELOEB,
    ROW_OUTPUT_RAADIGHEDSBELOEB,
    ROW_CURRENT_AVG_RAADIGHEDSBELOEB,
    # Afsavn
    ROW_CURRENT_FOEDSELSDAG,
    ROW_OUTPUT_FOEDSELSDAG,
    ROW_CURRENT_FRUGT_OG_GROENT,
    ROW_OUTPUT_FRUGT_OG_GROENT,
    ROW_CURRENT_TANDLAEGE,
    ROW_OUTPUT_TANDLAEGE,
    ROW_CURRENT_FRITID,
    ROW_OUTPUT_FRITID,
    ROW_CURRENT_FERIE,
    ROW_OUTPUT_FERIE,
    ROW_CURRENT_TOEJ,
    ROW_OUTPUT_TOEJ,
    ROW_CURRENT_FLERE_AFSAVN,
    ROW_OUTPUT_FLERE_AFSAVN,
) = (
    6, 7, 8,  # Kontanthjaelp
    11, 12, 13,  # Boligsikring
    14, 15, 16,  # Boerneydelse
    24, 26, 27,  # Fripladstilskud
    30, 31,  # Samlet ydelse
    39, 34,  # Rådighedsbeløb
    40,  # Gennemsnit rådighed for alle familier af type
    

    62, 63,  # Fødselsdag
    64, 65,  # Frugt og grønt
    66, 67,  # Tandlæge
    68, 69,  # Fritid
    70, 71,  # Ferie udenfor hjemmet
    72, 73,  # Tøj og sko
    74, 75,  # Flere afsavn
)


model_spreadsheet = resource_filename("beregner", "static/beregner/model.ods")

# Create static instances for pyoo so they don't have to be opened again and
# again
desktop = pyoo.Desktop('localhost', 2003)
doc = desktop.open_spreadsheet(model_spreadsheet)
main_sheet = doc.sheets[0]


def get_print_sheet(context):
    """Exposes the content of an .odt file so you can modify it."""
    
    tmp_file = tempfile.NamedTemporaryFile(suffix=".odt", mode="wb+")
    tmp_file.write(
        open(resource_filename("beregner", "templates/beregner/pdf/print_sheet.odt"), "rb").read(),
    )
    tmp_file.seek(0)
    odt_file = zipfile.ZipFile(tmp_file.name, "a")
    xml_string = odt_file.read('content.xml')
    xml_string = xml_string.replace(b'<text:soft-page-break/>', b'')
    tpl = Template(xml_string)
    html = tpl.render(Context(context))
    odt_file.writestr('content.xml', html.encode('utf-8'))
    odt_file.close()
    f_pdf = tempfile.NamedTemporaryFile(suffix=".pdf", mode="wb+")
    p = subprocess.Popen(['unoconv', '-f', 'pdf', '--stdout', '-d', 'document', tmp_file.name], stdout=f_pdf)
    p.communicate()
    f_pdf.seek(0)
    out = f_pdf.read()
    return HttpResponse(
        out,
        content_type='application/pdf',
    )


def close_doc(doc):
    doc.close()

atexit.register(close_doc, doc)


current_levels = {
    COL: {
        'kontanthj': main_sheet[ROW_CURRENT_KONTANTHJ, COL].value,
        'boligsikring': main_sheet[ROW_CURRENT_BOLIGSIKRING, COL].value,
        'boerneydelse': main_sheet[ROW_CURRENT_BOERNEYDELSE, COL].value,
        'fripladstilskud': main_sheet[ROW_CURRENT_FRIPLADSTILSKUD, COL].value,
        'samletydelse': main_sheet[ROW_CURRENT_SAMLET_YDELSE, COL].value,
        'raadighedsbeloeb': main_sheet[ROW_CURRENT_RAADIGHEDSBELOEB, COL].value,
        'raadighedsbeloeb_avg': main_sheet[ROW_CURRENT_AVG_RAADIGHEDSBELOEB, COL].value,

        'foedselsdag': main_sheet[ROW_CURRENT_FOEDSELSDAG, COL].value,
        'frugt_og_groent': main_sheet[ROW_CURRENT_FRUGT_OG_GROENT, COL].value,
        'tandlaege': main_sheet[ROW_CURRENT_TANDLAEGE, COL].value,
        'fritid': main_sheet[ROW_CURRENT_FRITID, COL].value,
        'ferie': main_sheet[ROW_CURRENT_FERIE, COL].value,
        'toej': main_sheet[ROW_CURRENT_TOEJ, COL].value,
        'flere_afsavn': main_sheet[ROW_CURRENT_FLERE_AFSAVN, COL].value,
    }
    for COL in (
        COL_ENLIGE_U_30_U_BOERN,
        COL_ENLIGE_O_30_U_BOERN,
        COL_ENLIGE_BARN_PAA_1,
        COL_BOERN_PAA_1_OG_5,
        COL_BOERN_PAA_1_5_10,
    )
}


def get_calculation(column_enum, input_kontanthj, input_boligsikring, input_boerneydelse, input_fripladstilskud):
    main_sheet[ROW_INPUT_KONTANTHJ, column_enum].value = input_kontanthj
    main_sheet[ROW_INPUT_BOLIGSIKRING, column_enum].value = input_boligsikring
    main_sheet[ROW_INPUT_BOERNEYDELSE, column_enum].value = input_boerneydelse
    main_sheet[ROW_INPUT_FRIPLADSTILSKUD, column_enum].value = input_fripladstilskud
    
    return {
        'kontanthj': main_sheet[ROW_OUTPUT_KONTANTHJ, column_enum].value,
        'boligsikring': main_sheet[ROW_OUTPUT_BOLIGSIKRING, column_enum].value,
        'boerneydelse': main_sheet[ROW_OUTPUT_BOERNEYDELSE, column_enum].value,
        'fripladstilskud': main_sheet[ROW_OUTPUT_FRIPLADSTILSKUD, column_enum].value,
        'samletydelse': main_sheet[ROW_OUTPUT_SAMLET_YDELSE, column_enum].value,
        'raadighedsbeloeb': main_sheet[ROW_OUTPUT_RAADIGHEDSBELOEB, column_enum].value,
        # Afsavn
        'foedselsdag': main_sheet[ROW_OUTPUT_FOEDSELSDAG, column_enum].value,
        'frugt_og_groent': main_sheet[ROW_OUTPUT_FRUGT_OG_GROENT, column_enum].value,
        'tandlaege': main_sheet[ROW_OUTPUT_TANDLAEGE, column_enum].value,
        'fritid': main_sheet[ROW_OUTPUT_FRITID, column_enum].value,
        'ferie': main_sheet[ROW_OUTPUT_FERIE, column_enum].value,
        'toej': main_sheet[ROW_OUTPUT_TOEJ, column_enum].value,
        'flere_afsavn': main_sheet[ROW_OUTPUT_FLERE_AFSAVN, column_enum].value,
    }
get_calculation = memoize(get_calculation, cache=CalculationCache(), num_args=5)
