from django.conf.urls import url

from . import views

urlpatterns = [
    url('^$', views.Calculator.as_view()),
    url('^iframe/$', views.CalculatorIframe.as_view(), name='iframe'),
]