# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import absolute_import
from django import forms
from django.utils.translation import ugettext_lazy as _

from . import utils


class ChangeField(forms.IntegerField):
    
    def __init__(self, *args, **kwargs):
        kwargs['initial'] = kwargs.get('initial', 0)
        kwargs['widget'] = kwargs.get('widget', forms.NumberInput(
            attrs={'style': 'width: 100px; text-align: right;', 'class': 'input'}
        ))
        super(ChangeField, self).__init__(*args, **kwargs)


class CalculationForm(forms.Form):
    
    FAMILY_TYPES = (
        (utils.COL_ENLIGE_U_30_U_BOERN, _("Enlig under 30 u/ børn")),
        (utils.COL_ENLIGE_O_30_U_BOERN, _("Enlig over 30 u/ børn")),
        (utils.COL_ENLIGE_BARN_PAA_1, _("Enlig m/ barn på 1 år")),
        (utils.COL_BOERN_PAA_1_OG_5, _("Par m. børn på 1 og 5 år")),
        (utils.COL_BOERN_PAA_1_5_10, _("Par m. børn på 1, 5 og 10 år")),
    )
    
    def __init__(self, *args, **kwargs):
        self.calc = None
        if not kwargs.get('data', None):
            self.current_levels = utils.current_levels[utils.COL_ENLIGE_U_30_U_BOERN]
        else:
            self.current_levels = utils.current_levels.get(
                int(kwargs['data'].get('column_enum', utils.COL_ENLIGE_U_30_U_BOERN))
            )
        super(CalculationForm, self).__init__(*args, **kwargs)
    
    pdf = forms.IntegerField(widget=forms.HiddenInput(), required=False, initial=0)
    
    column_enum = forms.TypedChoiceField(
        choices=FAMILY_TYPES,
        label=_("Gruppe"),
        initial=utils.COL_ENLIGE_U_30_U_BOERN,
        coerce=lambda x: int(x),
        empty_value=None,
    )
    kontanthj = ChangeField(
        label=_("Kontanthjælpen"),
    )
    boligsikring = ChangeField(
        label=_("Boligsikring"),
    )
    boerneydelse = ChangeField(
        label=_("Børneydelse"),
    )
    fripladstilskud = ChangeField(
        label=_("Fripladstilskud"),
    )

    def clean_kontanthj(self):
        cd = self.cleaned_data.get('kontanthj', 0)
        if cd >= 50000:
            raise forms.ValidationError("Beregningen er desværre ikke indrettet til stigninger af denne størrelse.")
        column_enum = self.cleaned_data.get('column_enum', utils.COL_ENLIGE_U_30_U_BOERN)
        min_value = utils.current_levels[column_enum]['kontanthj']
        if cd < -min_value:
            raise forms.ValidationError("Beregningen er desværre ikke indrettet til besparelser over {}.".format(min_value))
        return cd
    
    def clean(self):
        cd = self.cleaned_data
        if 'kontanthj' in cd and 'boligsikring' in cd and 'boerneydelse' in cd and 'fripladstilskud' in cd:
            self.calc = utils.get_calculation(
                cd['column_enum'],
                -cd['kontanthj'],
                -cd['boligsikring'],
                -cd['boerneydelse'],
                -cd['fripladstilskud'],
            )
        return cd
