# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import absolute_import

from django.views.generic.edit import FormView

from . import forms
from . import utils


class Calculator(FormView):
    
    form_class = forms.CalculationForm
    template_name = "beregner/base.html"
    
    def form_valid(self, form):
        if form.cleaned_data['pdf'] == 1:
            calc = form.calc
            current = form.current_levels
            return utils.get_print_sheet({
                'family_type': dict(form.FAMILY_TYPES)[form.cleaned_data['column_enum']],
                'input_kontanthj': calc['kontanthj'],
                'input_boligsikring': calc['boligsikring'],
                'input_boerneydelse': calc['boerneydelse'],
                'input_fripladstilskud': calc['fripladstilskud'],
                'input_samletydelse': calc['samletydelse'],

                'current_kontanthj': current['kontanthj'],
                'current_boligsikring': current['boligsikring'],
                'current_boerneydelse': current['boerneydelse'],
                'current_fripladstilskud': current['fripladstilskud'],
                'current_samletydelse': current['samletydelse'],
                
                'current_raadighedsbeloeb': current['raadighedsbeloeb'],
                'input_raadighedsbeloeb': calc['raadighedsbeloeb'],
                'avg_raadighedsbeloeb': current['raadighedsbeloeb_avg'],

                'input_foedselsdag': calc['foedselsdag'],
                'input_frugt_og_groent': calc['frugt_og_groent'],
                'input_tandlaege': calc['tandlaege'],
                'input_fritid': calc['fritid'],
                'input_ferie': calc['ferie'],
                'input_toej': calc['toej'],
                'input_flere_afsavn': calc['flere_afsavn'],

                'current_foedselsdag': current['foedselsdag'],
                'current_frugt_og_groent': current['frugt_og_groent'],
                'current_tandlaege': current['tandlaege'],
                'current_fritid': current['fritid'],
                'current_ferie': current['ferie'],
                'current_toej': current['toej'],
                'current_flere_afsavn': current['flere_afsavn'],
            })
        return self.render_to_response(self.get_context_data(form=form))


class CalculatorIframe(Calculator):
    
    template_name = "beregner/iframe.html"