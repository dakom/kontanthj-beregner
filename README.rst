Kontanthjælpsalliancens Kontanthjælpsberegner
=============================================

_`http://www.kontanthjælpsalliancen.dk/`

.. _`http://www.kontanthjælpsalliancen.dk/`: http://www.kontanthjælpsalliancen.dk/


Installation:

You need ``sudo apt-get install python3-uno unoconv libreoffice-common libreoffice-calc`` first.

NB! If this is deployed on Ubuntu 12.04, you need to use python 2.7 because
python 3.2 (shipped with Ubuntu 12.04) does not work with pyoo.

To run the libreoffice headless server, run ``soffice --accept="socket,host=localhost,port=2003;urp;" --norestore --nologo --nodefault --headless --calc``.

.. sourcecode :: bash

    # OPRET ET VIRTUALENV til Python 2
    mkvirtualenv kontant -p python2 --system-site-packages
    pip install -r requirements.txt
    python manage.py syncdb
    python manage.py createcachetable
    python manage.py runserver



Deployment
----------

You need to set ``env = HOME=/tmp`` or some other writable dir to have a
place where www-data and soffice can write temporary files.