import os
from contextlib import contextmanager as _contextmanager
from fabric.api import run, env
from fabric import api
from fabric.context_managers import cd, prefix

api.env.hosts = ['beregner@beregner.xn--kontanthjlpsalliancen-n3b.dk']

# NB! No trailing slashes
ENV_DIR = '/var/vhosts/kontanthj-beregner/venv'
PROJECT_DIR = '/var/vhosts/kontanthj-beregner/'

dirjoin = lambda x: os.path.join(PROJECT_DIR, x)

# Use the local .ssh/config
env.use_ssh_config = True

env.virtualenv = ENV_DIR
env.activate = 'source %(virtualenv)s/bin/activate' % env
env.code_dir = PROJECT_DIR


@_contextmanager
def virtualenv():
    with cd(env.virtualenv), prefix(env.activate), cd(env.code_dir):
        yield


def git_pull():
    """
    Pulls the latest master branch from letsgo-jernside git
    """

    with cd(PROJECT_DIR):
        run('git pull origin master')


def migrate():
    """
    Migrates the project
    """
    with virtualenv():
        run('python manage.py migrate')


def collectstatic():
    """
    Runs collectstatic on the remote project
    """
    with virtualenv():
        run('python manage.py collectstatic --noinput')


def deploy():
    """
    Runs hvitserk_git_pull, jernside_git_pull, migrate, and collectstatic
    """

    git_pull()
    migrate()
    collectstatic()
    api.run('touch /tmp/kontanthj-beregner.reload')
